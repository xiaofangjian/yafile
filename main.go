package main

import (
	"flag"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"path"
	"strings"

	"github.com/cloudreve/Cloudreve/v3/pkg/filesystem/driver/local"
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/xiaofangjian/yafile/pkg/cc"
	"github.com/xiaofangjian/yafile/pkg/conf"
	"github.com/xiaofangjian/yafile/pkg/fs"
	"github.com/xiaofangjian/yafile/pkg/gg"
)

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
}

func main() {
	ptrConfigPath := flag.String("c", "config.toml", "config file path")
	flag.Parse()

	log.Printf("loading config from %s", *ptrConfigPath)
	configContent := gg.Of(os.ReadFile(*ptrConfigPath)).Expect("read error")
	conf.LoadToml(configContent).Expect("decode toml config")

	g := gin.Default()
	g.MaxMultipartMemory = conf.AppConfig.Anonymous.MaxUploadBytes

	g.Delims("<<<", ">>>")
	g.LoadHTMLGlob("static/*.html")
	g.StaticFS("/static", http.Dir("static"))

	g.GET("/", Index)

	g.GET("/api/list", FileAPI(PathValidator[ListOut], APIList))
	g.POST("/api/list", FileAPI(PathValidator[ListOut], APIList))
	g.GET("/api/stat", FileAPI(PathValidator[StatOut], APIStat))
	g.POST("/api/stat", FileAPI(PathValidator[StatOut], APIStat))
	g.PUT("/api/put", FileAPI(APIPut))

	log.Println(g.Run(conf.AppConfig.Server.Addr))
}

func FileAPI[I, O any](nexts ...func(cc.Context, *fs.FileSystem, *I) (O, error)) func(*gin.Context) {
	return func(gc *gin.Context) {
		c := cc.FromKeys(gc.Keys)

		var in I
		_ = gc.ShouldBind(&in)

		useFs := GetFS()

		var o O
		var err error
		for _, next := range nexts {
			o, err = next(c, useFs, &in)
			if err != nil {
				writeHTTPJSON(gc, gg.Of(o, err))
				return
			}
		}

		writeHTTPJSON(gc, gg.Of(o, nil))
	}
}

func Index(c *gin.Context) {
	// temp := loadStaticFile("index.html")
	c.HTML(200, "index.html", gin.H{
		"config": conf.AppConfig,
	})
}

func BuildFS() *fs.FileSystem {
	cf := conf.AppConfig.Providers[conf.AppConfig.Provider.Use]
	switch cf["store"] {
	case "local":
		rootDir := cf["root"].(string)
		if !path.IsAbs(rootDir) {
			wd, _ := os.Getwd()
			rootDir = path.Join(wd, rootDir)
		}

		logrus.Infof("rootdirs=%s", rootDir)

		return &fs.FileSystem{
			Provider: &fs.CrProvider{
				Handler: local.Driver{},
			},
			RootDir: rootDir,
		}
	}

	return nil
}

func GetFS() *fs.FileSystem {
	return BuildFS()
}

type ListIn struct {
	File    string `json:"file" form:"file"`
	absFile string
}
type ListOut struct {
	FileInfos []fs.FileInfo `json:"file_infos"`
}

type StatOut struct {
	FileInfo fs.FileInfo `json:"file_info,omitempty"`
}

func PathValidator[O any](c cc.Context, useFs *fs.FileSystem, in *ListIn) (_ O, err error) {
	in.File = strings.Trim(in.File, " ")
	in.absFile = path.Join(useFs.RootDir, in.File)

	overoot := !strings.HasPrefix(in.absFile, useFs.RootDir)
	if overoot {
		err = errors.Errorf("over rootdir")
		return
	}

	return
}

func APIList(c cc.Context, useFs *fs.FileSystem, in *ListIn) (ListOut, error) {

	gr := useFs.List(c, in.absFile)
	if gr.Err() != nil {
		return ListOut{}, gr.Err()
	}

	return ListOut{
		FileInfos: gr.V(),
	}, nil
}

func APIStat(c cc.Context, useFs *fs.FileSystem, in *ListIn) (StatOut, error) {
	gr := useFs.Stat(c, in.absFile)
	if gr.Err() != nil {
		return StatOut{}, gr.Err()
	}

	return StatOut{
		FileInfo: gr.V(),
	}, nil
}

type PutIn struct {
	Files []multipart.FileHeader `form:"file[]"`

	Folder string `json:"folder"`
}

type PutOut struct {
	Tasks []*PutTask `json:"tasks" `
}

type PutTask struct {
	Filename string `json:"filename,omitempty"`
	Success  bool   `json:"success,omitempty"`
	ErrMsg   string `json:"err_msg,omitempty"`
}

func APIPut(c cc.Context, useFs *fs.FileSystem, in *PutIn) (r PutOut, err error) {
	absFolder := path.Join(useFs.RootDir, in.Folder)
	overoot := !strings.HasPrefix(absFolder, useFs.RootDir)
	if overoot {
		err = errors.Errorf("over rootdir")
		return
	}

	for _, fileIn := range in.Files {

		log.Println(fileIn.Filename)
		log.Println(fileIn.Size)
		log.Println(fileIn.Header)

		task := new(PutTask)
		r.Tasks = append(r.Tasks, task)

		fi, err := fileIn.Open()
		if err != nil {
			task.ErrMsg = err.Error()
			continue
		}

		gf := useFs.Upload(c, fileIn.Filename, in.Folder, uint64(fileIn.Size), fi)
		if gf.Err() != nil {
			task.ErrMsg = gf.Error()
			continue
		}

		task.Filename = gf.V().Name
		task.Success = true
	}

	return
}

func writeHTTPJSON(gc *gin.Context, r gg.Resulti) {
	if r.Err() != nil {
		// 拒绝处理
		gc.JSON(400, gin.H{"hint": r.Err().Error()})
		return
	}

	serializer(gc)(200, gin.H{"data": r.Vi()})
}

func serializer(gc *gin.Context) func(code int, obj any) {
	return gc.IndentedJSON
}
