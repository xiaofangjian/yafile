package cc

import (
	"context"

	"github.com/sirupsen/logrus"
)

type Context interface {
	context.Context

	Log() *logrus.Entry
}

func Background() Context {
	return FromContext(context.Background())
}

func TODO() Context {
	return FromContext(context.TODO())
}

func FromContext(c context.Context) Context {
	return &ContextImpl{c}
}

func FromKeys(keys map[string]any) Context {
	c := context.TODO()
	for k, v := range keys {
		c = context.WithValue(c, k, v)
	}

	return &ContextImpl{c}
}

type ContextImpl struct {
	context.Context
}

func (c *ContextImpl) Log() *logrus.Entry {
	return logrus.WithContext(c.Context)
}
