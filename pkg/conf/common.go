package conf

import (
	"github.com/BurntSushi/toml"
	"github.com/xiaofangjian/yafile/pkg/gg"
)

func LoadToml(tomlContent []byte) gg.Result[toml.MetaData] {
	return gg.Of(toml.Decode(string(tomlContent), &AppConfig))
}
