package conf

import "github.com/xiaofangjian/yafile/pkg/gg"

type Config struct {
	App       App       `toml:"app"`
	Server    Server    `toml:"server"`
	Provider  Provider  `toml:"provider"`
	Providers Providers `toml:"providers"`
	Anonymous Anonymous `toml:"anonymous"`
}

type App struct {
	Name        string `toml:"name"`
	Description string `toml:"description"`
}

type Server struct {
	Addr string `toml:"addr"`
}

type Provider struct {
	Use string `toml:"use"`
}

type Providers gg.D[gg.D[any]]

type Anonymous struct {
	Read           bool  `toml:"read"`
	Write          bool  `toml:"write"`
	MaxUploadBytes int64 `toml:"max_upload_bytes"`
}
