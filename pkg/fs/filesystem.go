package fs

import (
	"io"
	gfs "io/fs"
	"path"
	"time"

	"github.com/xiaofangjian/yafile/pkg/cc"
	"github.com/xiaofangjian/yafile/pkg/gg"
)

var _ gfs.FS
var _ gfs.File
var _ gfs.FileInfo
var _ gfs.FileMode
var _ gfs.DirEntry

var (
// _ gfs.FileInfo = (*FileInfo)(nil)
// _ gfs.DirEntry = DirInfo{}
)

type File struct {
	*FileInfo
	io.ReadSeekCloser

	VisiblePath string // path base fs.rootdir
}

type FileInfo struct {
	Name    string    `json:"name"`
	Size    uint64    `json:"size"`
	ModTime time.Time `json:"mod_time"`
	IsDir   bool      `json:"is_dir,omitempty"`
	// Mode    gfs.FileMode
}

type FileSystem struct {
	Provider Provider
	RootDir  string
}

func (fs *FileSystem) List(c cc.Context, name string) (result gg.Result[[]FileInfo]) {
	return fs.Provider.List(c, name)
}

func (fs *FileSystem) Stat(c cc.Context, name string) (result gg.Result[FileInfo]) {
	return fs.Provider.Stat(c, name)
}

func (fs *FileSystem) Read(c cc.Context, name string) (result gg.Result[File]) {
	return fs.Provider.Read(c, name)
}

func (fs *FileSystem) Upload(c cc.Context, filename string, folder string, size uint64, rsc io.ReadSeekCloser) (result gg.Result[File]) {
	f := File{
		FileInfo: &FileInfo{
			Name:    filename,
			Size:    size,
			ModTime: time.Now(),
			IsDir:   false,
		},
		ReadSeekCloser: rsc,
		VisiblePath:    path.Join(fs.RootDir, folder, filename),
	}

	return fs.Provider.Put(c, f)
}
