package fs

import (
	"path/filepath"

	"github.com/cloudreve/Cloudreve/v3/pkg/filesystem/driver"
	"github.com/cloudreve/Cloudreve/v3/pkg/filesystem/fsctx"
	"github.com/cloudreve/Cloudreve/v3/pkg/filesystem/response"
	"github.com/xiaofangjian/yafile/pkg/cc"
	"github.com/xiaofangjian/yafile/pkg/gg"
)

type Provider interface {
	List(ctx cc.Context, name string) gg.Result[[]FileInfo]
	Stat(ctx cc.Context, name string) gg.Result[FileInfo]
	Read(ctx cc.Context, name string) gg.Result[File]
	Put(ctx cc.Context, file File) gg.Result[File]
}

type CrProvider struct {
	driver.Handler
}

func (cr *CrProvider) List(ctx cc.Context, name string) gg.Result[[]FileInfo] {
	files := []FileInfo{}
	objs, err := cr.Handler.List(ctx, name, false)
	if err != nil {
		return gg.Of(files, err)
	}

	for _, obj := range objs {
		fi := Object2FileInfo(obj)
		files = append(files, fi)
	}

	return gg.Of(files, err)
}

func (cr *CrProvider) Stat(ctx cc.Context, name string) gg.Result[FileInfo] {
	fi := FileInfo{}
	objs, err := cr.Handler.List(ctx, filepath.Dir(name), false)
	if err != nil {
		return gg.Of(fi, err)
	}

	baseName := filepath.Base(name)
	for _, obj := range objs {
		if obj.Name != baseName {
			continue
		}

		fi = Object2FileInfo(obj)
		break
	}

	return gg.Of(fi, err)
}

func (cr *CrProvider) Read(ctx cc.Context, name string) gg.Result[File] {
	f := File{}
	rsc, err := cr.Handler.Get(ctx, name)
	if err != nil {
		return gg.Of(f, err)
	}

	f.ReadSeekCloser = rsc

	return gg.Of(f, nil)
}

func (cr *CrProvider) Put(ctx cc.Context, file File) gg.Result[File] {
	fstream := fsctx.FileStream{
		Mode:         fsctx.Overwrite,
		LastModified: &file.ModTime,
		Metadata:     nil,
		File:         file.ReadSeekCloser,
		Seeker:       file.ReadSeekCloser,
		Size:         file.Size,
		Name:         file.Name,
		SavePath:     file.VisiblePath,
		// VirtualPath:  "",
		// MIMEType:     "",
	}

	err := cr.Handler.Put(ctx, &fstream)
	if err != nil {
		return gg.Of(file, err)
	}

	return gg.Of(file, nil)
}

func Object2FileInfo(o response.Object) FileInfo {
	return FileInfo{
		Name:    o.Name,
		Size:    o.Size,
		ModTime: o.LastModify,
		IsDir:   o.IsDir,
	}
}
