package gg

type errorAware interface {
	WithErr(error)
}

type Resulti interface {
	Vi() any
	Err() error
}

type Result[T any] struct {
	v       T
	err     error
	errHint string
}

func (r Result[T]) V() T {
	return r.v
}

func (r Result[T]) Vi() any {
	return r.v
}

func (r Result[T]) Err() error {
	return r.err
}

func (r *Result[T]) WithErr(err error) {
	r.err = err
}

func (r Result[T]) Error() string {
	return r.err.Error()
}

func (r Result[T]) Expect(hint string) T {
	if r.err != nil {
		r.errHint = hint
		panic(r)
	}

	return r.v
}

func (r Result[T]) MustTo(ea errorAware) T {
	if r.err != nil {
		ea.WithErr(r.err)
		panic(r)
	}

	return r.v
}

func Of[T any](v T, err error) Result[T] {
	return Result[T]{v, err, ""}
}

func Err[T any](err error) Result[T] {
	return Result[T]{err: err}
}
